package Modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class ActorModel {
	private int id;
	private String nombre;
	private String apellido;

	public ActorModel(int id, String nombre, String apellido) {
		setId(id);
		setNombre(nombre);
		setApellido(apellido);
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public static ArrayList<ActorModel> obtenerActores() {
		ArrayList<ActorModel> actores = new ArrayList<>();
		try {
			Connection conn = (Connection) Singleton.getConnection();
			if (conn != null) {
				java.sql.Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery("select * from actor");
				while (rs.next()) {
					ActorModel actor = new ActorModel(rs.getInt("actor_id"), rs.getString("first_name"),
							rs.getString("last_name"));
					actores.add(actor);
				}

			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return actores;
	}

	public void insertarActor() {
		try {
			Connection conn = (Connection) Singleton.getConnection();
			if (conn != null) {
				java.sql.Statement st = conn.createStatement();
				String query = "INSERT INTO actor (actor_id, first_name, last_name) VALUES ('" + this.id + "', '"
						+ this.nombre + "', '" + this.apellido + "' )";
				st.executeUpdate(query);
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean borrarActor(int id) {
		boolean success = false;
		try {
			Connection conn = (Connection) Singleton.getConnection();
			if (conn != null) {
				java.sql.Statement st = conn.createStatement();
				String query = "DELETE FROM film_actor WHERE actor_id = '" + id + "'";
				String query1 = "DELETE FROM actor WHERE actor_id = '" + id + "'";
				st.executeUpdate(query);
				st.executeUpdate(query1);
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			success = true;
			JOptionPane.showMessageDialog(null, "No se pudo borrar");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			success = true;
			JOptionPane.showMessageDialog(null, "No se pudo borrar");
		}
		return success;
	}

	public boolean actualizarActor() {
		boolean success = false;
		try {
			Connection conn = (Connection) Singleton.getConnection();
			if (conn != null) {
				java.sql.Statement st = conn.createStatement();
				String query = "UPDATE actor SET first_name = '" + nombre + "',last_name = '" + apellido
						+ "' WHERE actor_id = '" + id + "' ";
				st.executeUpdate(query);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(null, "Debes rellenar todos los campos");
			success = true;
		}
		return success;
	}
}
