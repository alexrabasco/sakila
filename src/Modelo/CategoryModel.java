package Modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class CategoryModel {
	private int id;
	private String nombre;
	public CategoryModel(int id, String nombre) {
		setId(id);
		setNombre(nombre);
		
	}
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public static ArrayList<CategoryModel> obtenerCategorias() {
		ArrayList<CategoryModel> actores = new ArrayList<>();
		try {
			Connection conn = (Connection) Singleton.getConnection();
			if (conn != null) {
				java.sql.Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery("select * from category");
				while (rs.next()) {
					CategoryModel actor = new CategoryModel(rs.getInt("category_id"), rs.getString("name"));
					actores.add(actor);
				}

			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return actores;
	}
	public void insertarCategorias() {
		try {
			Connection conn =(Connection)Singleton.getConnection();
			if(conn!=null) {
				java.sql.Statement st = conn.createStatement();
				String query = "INSERT INTO category (category_id, name) VALUES ('"+this.id+"', '"+this.nombre+"' )";
				st.executeUpdate(query);
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public static boolean borrarCategoria(int id) {
		boolean success=false;
		try {
			Connection conn =(Connection)Singleton.getConnection();
			if(conn!=null) {
				java.sql.Statement st = conn.createStatement();
				String query = "DELETE FROM film_category where category_id='"+id+"'";
				String query1 = "DELETE FROM category where category_id='"+id+"'";
				st.executeUpdate(query);
				st.executeUpdate(query1);
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			success=true;
			JOptionPane.showMessageDialog(null, "No se pudo borrar");

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			success=true;
			JOptionPane.showMessageDialog(null, "No se pudo borrar");
		}
		return success;
	}
	public boolean actualizarCategoria() {
		boolean success=false;
		try {
			Connection conn = (Connection) Singleton.getConnection();
			if (conn != null) {
				java.sql.Statement st = conn.createStatement();
				String query = "UPDATE category SET name = '"+nombre+"' WHERE category_id = '"+id+"' ";
				st.executeUpdate(query);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(null, "Debes rellenar todos los campos");
			success=true;
		}
		return success;
	}
}
