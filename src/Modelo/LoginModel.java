package Modelo;

import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;


public class LoginModel {
	private String user;
	private String pass;
	public LoginModel(String user, String pass) {
		setUser(user);
		setPass(pass);
		
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user=user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass=pass;
	}
	public boolean httpRequest() {
		boolean v=false;
		try {
			URL url = new URL("https://dam.inasoft.es/login.php");
			HttpURLConnection con = (HttpURLConnection) url.openConnection();
			con.setRequestMethod("POST");
			HashMap<String, String> parameters = new HashMap<String, String>();
			parameters.put("user", this.getUser());
			parameters.put("password", this.getPass());
			con.setDoOutput(true);
			DataOutputStream out = new DataOutputStream(con.getOutputStream());
			out.writeBytes(ParameterStringBuilder.getParamsString(parameters));
			out.flush();
			out.close();
			if(con.getResponseCode()==200) {
				v=true;
				
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return v;
	}
}
