package Modelo;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLIntegrityConstraintViolationException;
import java.util.ArrayList;

import javax.swing.JOptionPane;


public class FilmModel {
	private int id;
	private String titulo;
	private String descripcion;
	private int anno;
	private int language;
	private int duracion;
	private float precioalquiler;
	private float precioventa;
	public FilmModel(int id, String titulo, String descripcion, int anno, int language, int duracion, float precioalquiler, float precioventa) {
		setId(id);
		setTitulo(titulo);
		setDescripcion(descripcion);
		setAnno(anno);
		setLanguage(language);
		setDuracion(duracion);
		setPrecioAlquiler(precioalquiler);
		setPrecioVenta(precioventa);
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id=id;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo=titulo;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion=descripcion;
	}
	public int getAnno() {
		return anno;
	}
	public void setAnno(int anno) {
		this.anno=anno;
	}
	public int getLanguage() {
		return language;
	}
	public void setLanguage(int language) {
		this.language=language;
	}
	public int getDuracion() {
		return duracion;
	}
	public void setDuracion(int duracion) {
		this.duracion=duracion;
	}
	public float getPrecioAlquiler() {
		return precioalquiler;
	}
	public void setPrecioAlquiler(float precioalquiler) {
		this.precioalquiler=precioalquiler;
	}
	public float getPrecioVenta() {
		return precioventa;
	}
	public void setPrecioVenta(float precioventa) {
		this.precioventa=precioventa;
	}
	
	public static ArrayList<FilmModel> obtenerPeliculas(){
		ArrayList<FilmModel> films = new ArrayList<>();
		try {
			Connection conn =(Connection)Singleton.getConnection();
			if(conn!=null) {
				java.sql.Statement st = conn.createStatement();
				ResultSet rs = st.executeQuery("select * from film");
				while(rs.next()) {
					FilmModel mf = new FilmModel(rs.getInt("film_id"), rs.getString("title"),rs.getString("description"),rs.getInt("release_year"), rs.getInt("language_id"), rs.getInt("length"), rs.getFloat("rental_rate"),rs.getFloat("replacement_cost"));
					films.add(mf);
					//System.out.println(rs.getInt("film_id")+rs.getString("title")+rs.getString("description")+rs.getInt("release_year"));
				}
				
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return films;
	}
	public void insertarFilm() {
		try {
			Connection conn =(Connection)Singleton.getConnection();
			if(conn!=null) {
				java.sql.Statement st = conn.createStatement();
				String query = "INSERT INTO film (film_id, title, description, release_year, language_id, length, rental_rate, replacement_cost) VALUES ('"+this.id+"', '"+this.titulo+"', '"+this.descripcion+"', '"+this.anno+"', '"+this.language+"', '"+this.duracion+"', '"+this.precioalquiler+"', '"+this.precioventa+"' )";
				st.executeUpdate(query);
			}
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean borrarPelicula(int id) {
		boolean success=false;
		try {
			Connection conn = (Connection) Singleton.getConnection();
			if (conn != null) {
				java.sql.Statement st = conn.createStatement();
				String query = "DELETE FROM film WHERE film_id = '" + id + "'";
				st.executeUpdate(query);
			}
		} catch (Exception ex) {
			JOptionPane.showMessageDialog(null, "No se puede borrar la película. Motivo: Tiene clave ajena");
			success=true;
		}
		return success;
	}
	public boolean actualizarPelicula() {
		boolean success=false;
		try {
			Connection conn = (Connection) Singleton.getConnection();
			if (conn != null) {
				java.sql.Statement st = conn.createStatement();
				String query = "UPDATE film SET title = '"+titulo+"', description = '"+descripcion+"', release_year = '"+anno+"', length = '"+duracion+"', rental_rate = '"+precioalquiler+"', replacement_cost = '"+precioventa+"' WHERE film_id = '"+id+"' ";
				st.executeUpdate(query);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			JOptionPane.showMessageDialog(null, "Debes rellenar todos los campos");
			success=true;
		}
		return success;
	}
	
}
