package Controlador;

import java.awt.Component;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ContainerEvent;
import java.awt.event.ContainerListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Modelo.ActorModel;
import Modelo.CategoryModel;
import Modelo.FilmModel;
import Vista.FormView;

public class UpdateController implements ActionListener, ListSelectionListener {
	private FormView formulario;
	private JTable tabla;
	private JButton boton;
	private JFrame ventana;
	public UpdateController(FormView formulario, JTable tabla, JButton boton, JFrame ventana ) {
		setFormulario(formulario);
		setTabla(tabla);
		setBoton(boton);
		setVentana(ventana);
		
	}
	public FormView getFormulario() {
		return formulario;
	}
	public void setFormulario(FormView formulario) {
		this.formulario=formulario;
	}
	public JTable getTabla() {
		return tabla;
	}
	public void setTabla(JTable tabla) {
		this.tabla=tabla;
	}
	public JButton getBoton() {
		return boton;
	}
	public void setBoton(JButton boton) {
		this.boton=boton;
	}
	public JFrame getVentana() {
		return ventana;
	}
	public void setVentana(JFrame ventana) {
		this.ventana=ventana;
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		//Mostramos la nueva ventana que se va a mostrar
		formulario.setVisible(true);
		
		int fila = this.tabla.getSelectedRow();
		int columnas = this.tabla.getColumnCount();
		
		if (columnas == 8) { //SI LA TABLA ES FILM
			for (Component componente: this.formulario.getContentPane().getComponents()) {
				if(componente instanceof JTextField) {
					//Id no se muestra visible ya que no podemos modificar el id
					if(componente.getName()=="tfid") {
						((JTextField) componente).setText(""+(Integer)this.tabla.getValueAt(fila, 0));
						((JTextField) componente).setEditable(false);

					}
					if(componente.getName()=="tftitulo") {
						((JTextField) componente).setText((String)this.tabla.getValueAt(fila, 1));
					}
					if(componente.getName()=="tfdescripcion") {
						((JTextField) componente).setText((String)this.tabla.getValueAt(fila, 2));
					}
					if(componente.getName()=="tfanno") {
						((JTextField) componente).setText(""+(Integer)this.tabla.getValueAt(fila, 3));
					}
					if(componente.getName()=="tfduracion") {
						((JTextField) componente).setText(""+(Integer)this.tabla.getValueAt(fila, 5));
					}
					if(componente.getName()=="tfprecioalquiler") {
						((JTextField) componente).setText(""+(Float)this.tabla.getValueAt(fila, 6));
					}
					if(componente.getName()=="tfprecioventa") {
						((JTextField) componente).setText(""+(Float)this.tabla.getValueAt(fila, 7));
					}
				}
				if(componente instanceof JComboBox) {
					if(componente.getName()=="comboidiomas") {
						componente.setVisible(false);
					}
					if(componente.getName()=="combotablas") {
						componente.setVisible(false);
					}
				}
				if(componente instanceof JLabel) {
					if(componente.getName()=="labelId") {
						componente.setVisible(true);
					}
					if(componente.getName()=="elegirtabla") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblLenguajeDePelicula") {
						componente.setVisible(false);
					}
					
				}
				if(componente instanceof JButton) {
					if(componente.getName()=="btnupdate") {
						componente.setVisible(true);
						((JButton) componente).addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								int id=0;
								String titulo="";
								String descripcion="";
								int anno=0;
								int language=0;//******<<<<<<<<<
								int duracion=0;
								float precioalquiler=0;
								float precioventa=0;
								
								for (Component componentee: formulario.getContentPane().getComponents()) {
									if(componentee instanceof JTextField) {
										//Id no se muestra visible ya que no podemos modificar el id
										if(componentee.getName()=="tfid") {
											id = Integer.parseInt(((JTextField) componentee).getText());
										}
										if(componentee.getName()=="tftitulo") {
											titulo = ((JTextField) componentee).getText();
										}
										if(componentee.getName()=="tfdescripcion") {
											descripcion = ((JTextField) componentee).getText();
										}
										if(componentee.getName()=="tfanno") {
											anno = Integer.parseInt(((JTextField) componentee).getText());
										}
										if(componentee.getName()=="tfduracion") {
											duracion = Integer.parseInt(((JTextField) componentee).getText());
										}
										if(componentee.getName()=="tfprecioalquiler") {
											precioalquiler = Float.parseFloat(((JTextField) componentee).getText());
										}
										if(componentee.getName()=="tfprecioventa") {
											precioventa = Float.parseFloat(((JTextField) componentee).getText());
										}
									
									}
									if (componentee instanceof JComboBox) {
										if(componentee.getName()=="comboidiomas") {
											language = ((JComboBox) componentee).getSelectedIndex();
										}
									}
								}
								FilmModel fm = new FilmModel(id, titulo, descripcion, anno, language, duracion, precioalquiler, precioventa);
								//llamamos al metodo actualizar de filmmodel
								fm.actualizarPelicula();
								if(fm.actualizarPelicula()==false) {
									JOptionPane.showMessageDialog(null, "Categoría actualizada satisfactoriamente");
									formulario.dispose();

								} else {
									JOptionPane.showMessageDialog(null, "Error al actualizar película");

								}
							}
							
						});
					}
					if(componente.getName()=="btninsertar") {
						componente.setVisible(false);
					}
					this.ventana.dispose();
				}
			}

		}

		if (columnas == 2) { //SI LA TABLA ES CATEGORIA
			for (Component componente: this.formulario.getContentPane().getComponents()) {
				if(componente instanceof JTextField) {
					//Id no se muestra visible ya que no podemos modificar el id
					if(componente.getName()=="tfid") {
						((JTextField) componente).setText(""+(Integer)this.tabla.getValueAt(fila, 0));
						((JTextField) componente).setEditable(false);

					}
					if(componente.getName()=="tftitulo") {
						((JTextField) componente).setText((String)this.tabla.getValueAt(fila, 1));
					}
					if(componente.getName()=="tfdescripcion") {
						componente.setVisible(false);
					}
					if(componente.getName()=="tfanno") {
						componente.setVisible(false);
					}
					if(componente.getName()=="tfduracion") {
						componente.setVisible(false);
					}
					if(componente.getName()=="tfprecioalquiler") {
						componente.setVisible(false);
					}
					if(componente.getName()=="tfprecioventa") {
						componente.setVisible(false);
					}
				}
				if(componente instanceof JComboBox) {
					if(componente.getName()=="comboidiomas") {
						componente.setVisible(false);
					}
					if(componente.getName()=="combotablas") {
						componente.setVisible(false);
					}
				}
				if(componente instanceof JLabel) {
					if(componente.getName()=="labelId") {
						componente.setVisible(true);
					}
					if(componente.getName()=="lblTitulo") {
						componente.setName("Nombre");
					}
					if(componente.getName()=="elegirtabla") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblLenguajeDePelicula") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblprecioventa") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblalquiler") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblduracion") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblAoDeLanzamiento") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblDescripcion") {
						componente.setVisible(false);
					}
					
				}
				if(componente instanceof JButton) {
					if(componente.getName()=="btnupdate") {
						componente.setVisible(true);
						((JButton) componente).addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								int id=0;
								String nombre="";
								
								for (Component componentee: formulario.getContentPane().getComponents()) {
									if(componentee instanceof JTextField) {
										//Id no se muestra visible ya que no podemos modificar el id
										if(componentee.getName()=="tfid") {
											id = Integer.parseInt(((JTextField) componentee).getText());
										}
										if(componentee.getName()=="tftitulo") {
											nombre = ((JTextField) componentee).getText();
										}
									
									}
								}
								CategoryModel cm = new CategoryModel(id, nombre);
								//llamamos al metodo actualizar 
								cm.actualizarCategoria();
								if(cm.actualizarCategoria()==false) {
									JOptionPane.showMessageDialog(null, "Categoría actualizada satisfactoriamente");
									formulario.dispose();
								} else {
									JOptionPane.showMessageDialog(null, "No se pudo actualizar la categoría!");
								}
							}
						});
					}
					if(componente.getName()=="btninsertar") {
						componente.setVisible(false);
					}
					this.ventana.dispose();
				}
			}

		}

		if (columnas == 3) { // SI LA TABLA ES ACTOR
			for (Component componente: this.formulario.getContentPane().getComponents()) {
				if(componente instanceof JTextField) {
					//Id no se muestra visible ya que no podemos modificar el id
					if(componente.getName()=="tfid") {
						((JTextField) componente).setText(""+(Integer)this.tabla.getValueAt(fila, 0));
						((JTextField) componente).setEditable(false);

					}
					if(componente.getName()=="tftitulo") {
						((JTextField) componente).setText((String)this.tabla.getValueAt(fila, 1));
					}
					if(componente.getName()=="tfdescripcion") {
						((JTextField) componente).setText((String)this.tabla.getValueAt(fila, 1));
					}
					if(componente.getName()=="tfanno") {
						componente.setVisible(false);
					}
					if(componente.getName()=="tfduracion") {
						componente.setVisible(false);
					}
					if(componente.getName()=="tfprecioalquiler") {
						componente.setVisible(false);
					}
					if(componente.getName()=="tfprecioventa") {
						componente.setVisible(false);
					}
				}
				if(componente instanceof JComboBox) {
					if(componente.getName()=="comboidiomas") {
						componente.setVisible(false);
					}
					if(componente.getName()=="combotablas") {
						componente.setVisible(false);
					}
				}
				if(componente instanceof JLabel) {
					if(componente.getName()=="labelId") {
						componente.setVisible(true);
					}
					if(componente.getName()=="lblTitulo") {
						componente.setName("Nombre");
					}
					if(componente.getName()=="elegirtabla") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblLenguajeDePelicula") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblprecioventa") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblalquiler") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblduracion") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblAoDeLanzamiento") {
						componente.setVisible(false);
					}
					if(componente.getName()=="lblDescripcion") {
						componente.setVisible(true);
						componente.setName("Apellido");
					}
					
				}
				if(componente instanceof JButton) {
					if(componente.getName()=="btnupdate") {
						componente.setVisible(true);
						((JButton) componente).addActionListener(new ActionListener() {
							
							@Override
							public void actionPerformed(ActionEvent e) {
								int id=0;
								String nombre="";
								String apellido="";
								
								for (Component componentee: formulario.getContentPane().getComponents()) {
									if(componentee instanceof JTextField) {
										//Id no se muestra visible ya que no podemos modificar el id
										if(componentee.getName()=="tfid") {
											id = Integer.parseInt(((JTextField) componentee).getText());
										}
										if(componentee.getName()=="tftitulo") {
											nombre = ((JTextField) componentee).getText();
										}
										if(componentee.getName()=="tfdescripcion") {
											apellido = ((JTextField) componentee).getText();
										}
									
									}
								}
								ActorModel am = new ActorModel(id, nombre, apellido);
								//llamamos al metodo actualizar
								am.actualizarActor();
								if(am.actualizarActor()==false) {
									JOptionPane.showMessageDialog(null, "Categoría actualizada satisfactoriamente");
									formulario.dispose();
								} else {
									JOptionPane.showMessageDialog(null, "No se pudo actualizar la categoría!");
								}
							}
						});
					}
					if(componente.getName()=="btninsertar") {
						componente.setVisible(false);
					}
					this.ventana.dispose();
				}
			}

		}
		//int fila = this.tabla.getSelectedRow();
		
	}
	@Override
	public void valueChanged(ListSelectionEvent e) {
		if(this.tabla.getSelectedRow()==-1) {
			this.boton.setEnabled(false);
		} else {
			this.boton.setEnabled(true);
		}
		
	}

}
