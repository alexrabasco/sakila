package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Modelo.ActorModel;

public class ActorController implements ActionListener, ItemListener {
	private JTable tabla;
	private JLabel titulo;
	private JComboBox<String> numregistros;
	private JButton siguiente;
	private JButton atras;
	private JButton ultimo;
	private JButton primero;
	private JButton siguientefilm;
	private JButton atrasfilm;
	private JButton ultimofilm;
	private JButton primerofilm;
	private JButton siguientecategory;
	private JButton atrascategory;
	private JButton ultimocategory;
	private JButton primerocategory;
	int paginaactual;
	public ActorController(JTable tabla, JLabel titulo, JComboBox<String> numregistros,JButton  siguiente,  JButton atras, JButton ultimo,
			JButton primero,  JButton  siguientefilm,  JButton atrasfilm, JButton ultimofilm, JButton primerofilm, 
			JButton  siguientecategory,  JButton atrascategory, JButton ultimocategory, JButton primerocategory) {
		setTabla(tabla);
		setTitulo(titulo);
		setNumRegistros(numregistros);
		
		setSiguiente(siguiente);
		setAtras(atras);
		setPrimero(primero);
		setUltimo(ultimo);
		
		setSiguientefilm(siguientefilm);
		setAtrasfilm(atrasfilm);
		setPrimerofilm(primerofilm);
		setUltimofilm(ultimofilm);
		
		setSiguientecategory(siguientecategory);
		setAtrascategory(atrascategory);
		setPrimerocategory(primerocategory);
		setUltimocategory(ultimocategory);
	}
	public JTable getTabla() {
		return tabla;
	}
	public void setTabla(JTable tabla) {
		this.tabla=tabla;
	}
	public JLabel getTitulo() {
		return titulo;
	}
	public void setTitulo(JLabel titulo) {
		this.titulo=titulo;
	}
	public JComboBox<String> getNumRegistros(){
		return numregistros;
	}
	public void setNumRegistros(JComboBox<String> numregistros) {
		this.numregistros=numregistros;
	}
	//**********************************
	public JButton getSiguiente() {
		return siguiente;
	}
	public void setSiguiente( JButton siguiente ) {
		this.siguiente=siguiente;
	}
	public JButton getAtras() {
		return atras;
	}
	public void setAtras(JButton atras) {
		this.atras=atras;
	}
	public JButton getPrimero() {
		return primero;
	}
	public void setPrimero(JButton primero) {
		this.primero=primero;
	}
	public JButton getUltimo() {
		return ultimo;
	}
	public void setUltimo(JButton ultimo) {
		this.ultimo=ultimo;
	}
	//*********************************
	public JButton getSiguientefilm() {
		return siguientefilm;
	}
	public void setSiguientefilm( JButton siguientefilm ) {
		this.siguientefilm=siguientefilm;
	}
	public JButton getAtrasfilm() {
		return atrasfilm;
	}
	public void setAtrasfilm(JButton atrasfilm) {
		this.atrasfilm=atrasfilm;
	}
	public JButton getPrimerofilm() {
		return primerofilm;
	}
	public void setPrimerofilm(JButton primerofilm) {
		this.primerofilm=primerofilm;
	}
	public JButton getUltimofilm() {
		return ultimofilm;
	}
	public void setUltimofilm(JButton ultimofilm) {
		this.ultimofilm=ultimofilm;
	}
	//*********************************
	public JButton getSiguientecategory() {
		return siguientecategory;
	}
	public void setSiguientecategory( JButton siguientecategory ) {
		this.siguientecategory=siguientecategory;
	}
	public JButton getAtrasfilmcategory() {
		return atrascategory;
	}
	public void setAtrascategory(JButton atrascategory) {
		this.atrascategory=atrascategory;
	}
	public JButton getPrimerocategory() {
		return primerocategory;
	}
	public void setPrimerocategory(JButton primerocategory) {
		this.primerocategory=primerocategory;
	}
	public JButton getUltimocategory() {
		return ultimocategory;
	}
	public void setUltimocategory(JButton ultimocategory) {
		this.ultimocategory=ultimocategory;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getActionCommand()=="Tabla actores") {
			this.siguientecategory.setVisible(false);
			this.atrascategory.setVisible(false);
			this.ultimocategory.setVisible(false);
			this.primerocategory.setVisible(false);
			
			this.siguientefilm.setVisible(false);
			this.atrasfilm.setVisible(false);
			this.ultimofilm.setVisible(false);
			this.primerofilm.setVisible(false);
			
			this.siguiente.setVisible(true);
			this.atras.setVisible(true);
			this.ultimo.setVisible(true);
			this.primero.setVisible(true);
			System.out.println("holaa");
		}
		//mostramos visibles los botones que nos interesen y los que no los ocultamos

		
		
		DefaultTableModel model = new DefaultTableModel();
		this.titulo.setText("Lista de actores");
		this.tabla.removeAll();
		this.tabla.setModel(model);
		
		model.addColumn("Id");
		model.addColumn("Nombre");
		model.addColumn("Apellido");
		ArrayList<ActorModel> actores = ActorModel.obtenerActores();
		//variables
		this.atras.setEnabled(false);
		int registros = actores.size();
		int numregistros = Integer.parseInt(this.numregistros.getSelectedItem().toString());
		int paginas=registros/numregistros;
		if(paginaactual!=0) {
			this.siguiente.setEnabled(true);
		}
		if(paginaactual!=0) {
			this.atras.setEnabled(true);
		}

		//*********
		for(int i=0;i<10;i++) {
			Object[] datos = new Object[]{actores.get(i).getId(),actores.get(i).getNombre(),actores.get(i).getApellido()};
			model.addRow(datos);
		}
		
		if(e.getActionCommand()==">") {
			System.out.println(model.getRowCount());
			while(model.getRowCount()!=0) {
				model.removeRow(0);
			}
			paginaactual++;
			
			for(int i=numregistros*paginaactual, nregistros=i+numregistros;i<nregistros;i++) {
				Object[] datos = new Object[]{actores.get(i).getId(),actores.get(i).getNombre(),actores.get(i).getApellido()};
				model.addRow(datos);
				if(i>=registros-1) {
					break;
				}
			}
			if(paginaactual==paginas) {
				this.siguiente.setEnabled(false);
			}
			this.atras.setEnabled(true);
			System.out.println(paginaactual);
		}
		if(e.getActionCommand()=="<") {
			System.out.println(model.getRowCount());
			while(model.getRowCount()!=0) {
				model.removeRow(0);
			}
			paginaactual--;
			
			for(int i=numregistros*paginaactual, nregistros=i+numregistros;i<nregistros;i++) {
				Object[] datos = new Object[]{actores.get(i).getId(),actores.get(i).getNombre(),actores.get(i).getApellido()};
				model.addRow(datos);
				if(i>=registros-1) {
					break;
				}
			}
			if(paginaactual==0) {
				this.atras.setEnabled(false);
			}
		}
		if(e.getActionCommand()==">>") {
			while(model.getRowCount()!=0) {
				model.removeRow(0);
			}
			paginaactual=paginas;
			
			for(int i=numregistros*paginaactual, nregistros=i+numregistros;i<nregistros;i++) {
				Object[] datos = new Object[]{actores.get(i).getId(),actores.get(i).getNombre(),actores.get(i).getApellido()};
				model.addRow(datos);
				if(i>=registros-1) {
					break;
				}
			}
				this.siguiente.setEnabled(false);
				this.atras.setEnabled(true);
		}
		if(e.getActionCommand()=="<<") {
			while(model.getRowCount()!=0) {
				model.removeRow(0);
			}
			paginaactual=0;
			
			for(int i=numregistros*paginaactual, nregistros=i+numregistros;i<nregistros;i++) {
				Object[] datos = new Object[]{actores.get(i).getId(),actores.get(i).getNombre(),actores.get(i).getApellido()};
				model.addRow(datos);
				if(i>=registros) {
					break;
				}
				
			}
				this.atras.setEnabled(false);
		}
		
	}
	@Override
	public void itemStateChanged(ItemEvent e) {

	}

}
