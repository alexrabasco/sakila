package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.text.html.FormView;

import Modelo.ActorModel;
import Modelo.CategoryModel;
import Modelo.FilmModel;

public class InsertController implements ActionListener, ItemListener {
	private JComboBox<String> tabla;
	private JComboBox<String> idiomas;
	private JLabel id;
	private JLabel nombre;
	private JLabel descripcion;
	private JLabel anno;
	private JLabel tituloidiomas;
	private JTextField txtid;
	private JTextField txtNombre;
	private JTextField txtDescripcion;
	private JTextField txtAnno;
	private JLabel duracion;
	private JLabel precioalquiler;
	private JLabel precioventa;
	private JTextField tfduracion;
	private JTextField tfprecioalquiler;
	private JTextField tfprecioventa;
	private JFrame ventana;
	
	public InsertController(JComboBox<String> tabla,JComboBox<String> idiomas, JLabel id, JLabel nombre, JLabel descripcion, JLabel anno, JTextField txtid, JTextField txtNombre, 
			JTextField txtDescripcion, JTextField txtAnno, JLabel tituloidiomas, JLabel duracion, JLabel precioalquiler, JLabel precioventa, JTextField tfduracion,
			JTextField tfprecioalquiler, JTextField tfprecioventa, JFrame ventana) {
		setTabla(tabla);
		setIdiomas(idiomas);
		setId(id);
		setNombre(nombre);
		setDescripcion(descripcion);
		setAnno(anno);
		setTxtid(txtid);
		setTxtnombre(txtNombre);
		setTxtdescripcion(txtDescripcion);
		setTxtanno(txtAnno);
		setTituloidiomas(tituloidiomas);
		setDuracion(duracion);
		setPrecioAlquiler(precioalquiler);
		setPrecioVenta(precioventa);
		setTFduracion(tfduracion);
		setTFprecioalquiler(tfprecioalquiler);
		setTFprecioventa(tfprecioventa);
		setVentana(ventana);
		
	}
	public JComboBox<String> getTabla(){
		return tabla;
	}
	public void setTabla(JComboBox<String> tabla) {
		this.tabla=tabla;
	}
	public JComboBox<String> getIdiomas(){
		return idiomas;
	}
	public void setIdiomas(JComboBox<String> idiomas) {
		this.idiomas=idiomas;
	}
	//*******************************
	public JLabel getId() {
		return id;
	}
	public void setId(JLabel id) {
		this.id=id;
	}
	public JLabel getNombre() {
		return nombre;
	}
	public void setNombre(JLabel nombre) {
		this.nombre=nombre;
	}
	public JLabel getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(JLabel descripcion) {
		this.descripcion=descripcion;
	}
	public JLabel getAnno() {
		return anno;
	}
	public void setAnno(JLabel anno) {
		this.anno=anno;
	}
	//*******************************
	public JTextField getTxtid() {
		return txtid;
	}
	public void setTxtid(JTextField txtid) {
		this.txtid=txtid;
	}
	public JTextField getTxtnombre() {
		return txtNombre;
	}
	public void setTxtnombre(JTextField txtNombre) {
		this.txtNombre=txtNombre;
	}
	public JTextField getTxtdescripcion() {
		return txtDescripcion;
	}
	public void setTxtdescripcion(JTextField txtDescripcion) {
		this.txtDescripcion=txtDescripcion;
	}
	public JTextField getTxtanno() {
		return txtAnno;
	}
	public void setTxtanno(JTextField txtAnno) {
		this.txtAnno=txtAnno;
	}
	public JLabel getTituloidiomas() {
		return tituloidiomas;
	}
	public void setTituloidiomas(JLabel tituloidiomas) {
		this.tituloidiomas=tituloidiomas;
	}
	public JLabel getDuracion() {
		return duracion;
	}
	public void setDuracion(JLabel duracion) {
		this.duracion=duracion;
	}
	public JLabel getPrecioAlquiler() {
		return precioalquiler;
	}
	public void setPrecioAlquiler(JLabel precioalquiler) {
		this.precioalquiler=precioalquiler;
	}
	public JLabel getPrecioVenta() {
		return precioventa;
	}
	public void setPrecioVenta(JLabel precioventa) {
		this.precioventa=precioventa;
	}
	public JTextField getTFduracion() {
		return tfduracion;
	}
	public void setTFduracion(JTextField tfduracion) {
		this.tfduracion=tfduracion;
	}
	public JTextField getTFprecioalquiler() {
		return tfprecioalquiler;
	}
	public void setTFprecioalquiler(JTextField tfprecioalquiler) {
		this.tfprecioalquiler=tfprecioalquiler;
	}
	public JTextField getTFprecioventa() {
		return tfprecioventa;
	}
	public void setTFprecioventa(JTextField tfprecioventa) {
		this.tfprecioventa=tfprecioventa;
	}
	public JFrame getVentana() {
		return ventana;
	}
	public void setVentana(JFrame ventana) {
		this.ventana=ventana;
	}
	

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println(e.getActionCommand());
		if (e.getActionCommand().equals("Insertar")) {

			if (this.tabla.getSelectedItem().toString().equals("film")) {
				int id = Integer.parseInt(this.txtid.getText());
				int anno = Integer.parseInt(this.txtAnno.getText());
				int duracion = Integer.parseInt(this.getTFduracion().getText());
				float precioalq = Float.parseFloat(this.getTFprecioalquiler().getText());
				float preciovent = Float.parseFloat(this.getTFprecioventa().getText());

				// String item = (String) this.idiomas.getSelectedItem().toString();
				int idiomaseleccionado;
				switch (this.idiomas.getSelectedItem().toString()) {
				case "English":
					idiomaseleccionado = 1;
					break;
				case "Italian":
					idiomaseleccionado = 2;
					break;
				case "Japanese":
					idiomaseleccionado = 3;
					break;
				case "Mandarin":
					idiomaseleccionado = 4;
					break;
				case "French":
					idiomaseleccionado = 5;
					break;
				case "German":
					idiomaseleccionado = 6;
					break;
				default:
					idiomaseleccionado = 1;
					break;
				}

				FilmModel mf = new FilmModel(id, this.txtNombre.getText(), this.txtDescripcion.getText(), anno,
						idiomaseleccionado, duracion, precioalq, preciovent);
				mf.insertarFilm();
				// mf.insertarFilm();

			}

			if (this.tabla.getSelectedItem().toString().equals("actor")) {
				int id = Integer.parseInt(this.txtid.getText());
				ActorModel am = new ActorModel(id, this.txtNombre.getText(), this.txtDescripcion.getText());
				am.insertarActor();

			}

			if (this.tabla.getSelectedItem().toString().equals("category")) {
				int id = Integer.parseInt(this.txtid.getText());
				CategoryModel cm = new CategoryModel(id, this.txtNombre.getText());
				cm.insertarCategorias();

			}
			this.ventana.dispose();
		}

	}
	@Override
	public void itemStateChanged(ItemEvent e) {
		switch (this.tabla.getSelectedItem().toString()) {
		case "film":
			this.id.setText("Id");
			this.nombre.setText("Titulo");
			this.descripcion.setText("Descripcion");
			this.anno.setText("Año de lanzamiento");
			txtDescripcion.setVisible(true);
			txtAnno.setVisible(true);
			this.descripcion.setVisible(true);
			this.anno.setVisible(true);
			this.idiomas.setVisible(true);
			this.tituloidiomas.setVisible(true);
			this.descripcion.setVisible(true);
			this.duracion.setVisible(true);
			this.tfduracion.setVisible(true);
			this.precioalquiler.setVisible(true);
			this.tfprecioalquiler.setVisible(true);
			this.precioventa.setVisible(true);
			this.tfprecioventa.setVisible(true);
			break;
			
		case "actor":
			this.nombre.setText("Nombre del actor");
			this.descripcion.setText("Apellido");
			this.anno.setVisible(false);
			this.txtAnno.setVisible(false);
			this.idiomas.setVisible(false);
			this.tituloidiomas.setVisible(false);
			this.descripcion.setVisible(true);
			this.descripcion.setVisible(true);
			this.duracion.setVisible(false);
			this.tfduracion.setVisible(false);
			this.precioalquiler.setVisible(false);
			this.tfprecioalquiler.setVisible(false);
			this.precioventa.setVisible(false);
			this.tfprecioventa.setVisible(false);
			break;
			
		case "category":
			this.nombre.setText("Nombre de la categoría");
			this.descripcion.setVisible(false);
			this.txtDescripcion.setVisible(false);
			this.anno.setVisible(false);
			this.txtAnno.setVisible(false);
			this.idiomas.setVisible(false);
			this.tituloidiomas.setVisible(false);
			this.descripcion.setVisible(false);
			this.duracion.setVisible(false);
			this.tfduracion.setVisible(false);
			this.precioalquiler.setVisible(false);
			this.tfprecioalquiler.setVisible(false);
			this.precioventa.setVisible(false);
			this.tfprecioventa.setVisible(false);
			
			

		default:
			break;
		}
		
	}

}
