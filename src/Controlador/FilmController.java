package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Modelo.FilmModel;

public class FilmController implements ActionListener {
	private JTable tabla;
	private JLabel titulo;
	private JComboBox<String> numregistros;
	private JButton siguiente;
	private JButton atras;
	private JButton ultimo;
	private JButton primero;
	private JButton siguienteactor;
	private JButton atrasactor;
	private JButton ultimoactor;
	private JButton primeroactor;
	private JButton siguientecategory;
	private JButton atrascategory;
	private JButton ultimocategory;
	private JButton primerocategory;
	int paginaactual;
//añadir todos los botones de siguiente etc
	public FilmController(JTable tabla, JLabel titulo,  JComboBox<String> numregistros,  JButton  siguiente,  JButton atras, JButton ultimo, JButton primero,
			JButton  siguienteactor,  JButton atrasactor, JButton ultimoactor, JButton primeroactor,
			JButton  siguientecategory,  JButton atrascategory, JButton ultimocategory, JButton primerocategory) {
		setTable(tabla);
		setTitulo(titulo);
		setNumRegistros(numregistros);
		setSiguiente(siguiente);
		setAtras(atras);
		setPrimero(primero);
		setUltimo(ultimo);
		
		setSiguienteactor(siguienteactor);
		setAtrasactor(atrasactor);
		setPrimeroactor(primeroactor);
		setUltimoactor(ultimoactor);
		
		setSiguientecategory(siguientecategory);
		setAtrascategory(atrascategory);
		setPrimerocategory(primerocategory);
		setUltimocategory(ultimocategory);
		

	}

	public JTable getTable() {
		return tabla;
	}

	public void setTable(JTable tabla) {
		this.tabla = tabla;
	}

	public JLabel getTitulo() {
		return titulo;
	}

	public void setTitulo(JLabel titulo) {
		this.titulo = titulo;
	}
	public JComboBox<String> getNumRegistros(){
		return numregistros;
	}
	public void setNumRegistros(JComboBox<String> numregistros) {
		this.numregistros=numregistros;
	}
	public JButton getSiguiente() {
		return siguiente;
	}
	public void setSiguiente( JButton siguiente ) {
		this.siguiente=siguiente;
	}
	public JButton getAtras() {
		return atras;
	}
	public void setAtras(JButton atras) {
		this.atras=atras;
	}
	public JButton getPrimero() {
		return primero;
	}
	public void setPrimero(JButton primero) {
		this.primero=primero;
	}
	public JButton getUltimo() {
		return ultimo;
	}
	public void setUltimo(JButton ultimo) {
		this.ultimo=ultimo;
	}
	//********************************
	public JButton getSiguienteactor() {
		return siguienteactor;
	}
	public void setSiguienteactor( JButton siguienteactor ) {
		this.siguienteactor=siguienteactor;
	}
	public JButton getAtrasactor() {
		return atrasactor;
	}
	public void setAtrasactor(JButton atrasactor) {
		this.atrasactor=atrasactor;
	}
	public JButton getPrimeroactor() {
		return primeroactor;
	}
	public void setPrimeroactor(JButton primeroactor) {
		this.primeroactor=primeroactor;
	}
	public JButton getUltimoactor() {
		return ultimoactor;
	}
	public void setUltimoactor(JButton ultimoactor) {
		this.ultimoactor=ultimoactor;
	}
	//********************************
	public JButton getSiguientecategory() {
		return siguientecategory;
	}
	public void setSiguientecategory( JButton siguientecategory ) {
		this.siguientecategory=siguientecategory;
	}
	public JButton getAtrasfilmcategory() {
		return atrascategory;
	}
	public void setAtrascategory(JButton atrascategory) {
		this.atrascategory=atrascategory;
	}
	public JButton getPrimerocategory() {
		return primerocategory;
	}
	public void setPrimerocategory(JButton primerocategory) {
		this.primerocategory=primerocategory;
	}
	public JButton getUltimocategory() {
		return ultimocategory;
	}
	public void setUltimocategory(JButton ultimocategory) {
		this.ultimocategory=ultimocategory;
	}

	public void listar() {
		//Para que al acceder al index nos muestre ya directamente la tabla films
		DefaultTableModel model = new DefaultTableModel();
		this.tabla.setModel(model);
		//Cambiamos el actionlistener del botón ya que los estamos reutilizando

		this.siguiente.addActionListener(this);
		this.atras.addActionListener(this);
		this.primero.addActionListener(this);
		this.ultimo.addActionListener(this);
		model.addColumn("Id");
		model.addColumn("Titulo");
		model.addColumn("Descripción");
		model.addColumn("Año de salida");
		model.addColumn("Idioma");
		model.addColumn("Duración");
		model.addColumn("Precio alquiler");
		model.addColumn("Precio venta");
		ArrayList<FilmModel> films = FilmModel.obtenerPeliculas();
		for (int i = 1; i < 10; i++) {
			Object[] datos = new Object[] { films.get(i).getId(), films.get(i).getTitulo(),
					films.get(i).getDescripcion(), films.get(i).getAnno(),films.get(i).getLanguage(),films.get(i).getDuracion(),films.get(i).getPrecioAlquiler(),films.get(i).getPrecioVenta() };
			model.addRow(datos);
		}
		this.numregistros.setSelectedItem("10");
		this.atras.setEnabled(false);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		
		this.siguientecategory.setVisible(false);
		this.atrascategory.setVisible(false);
		this.ultimocategory.setVisible(false);
		this.primerocategory.setVisible(false);
		
		this.siguienteactor.setVisible(false);
		this.atrasactor.setVisible(false);
		this.ultimoactor.setVisible(false);
		this.primeroactor.setVisible(false);
		
		this.siguiente.setVisible(true);
		this.atras.setVisible(true);
		this.ultimo.setVisible(true);
		this.primero.setVisible(true);
		
		//Para que al dar al boton de listar películas nos vuelva a mostrar todas
		this.titulo.setText("Lista de películas");
		DefaultTableModel model = new DefaultTableModel();
		this.tabla.removeAll();
		this.tabla.setModel(model);
		model.addColumn("Id");
		model.addColumn("Titulo");
		model.addColumn("Descripción");
		model.addColumn("Año de salida");
		model.addColumn("Idioma");
		model.addColumn("Duración");
		model.addColumn("Precio alquiler");
		model.addColumn("Precio venta");
		ArrayList<FilmModel> films = FilmModel.obtenerPeliculas();
		
		this.atras.setEnabled(false);
		int registros = films.size();
		int numregistros = Integer.parseInt(this.numregistros.getSelectedItem().toString());
		int paginas=registros/numregistros;
		if(paginaactual!=0) {
			this.siguiente.setEnabled(true);
		}
		if(paginaactual!=0) {
			this.atras.setEnabled(true);
		}

		//*********
		for(int i=0;i<10;i++) {
			Object[] datos = new Object[] { films.get(i).getId(), films.get(i).getTitulo(),
					films.get(i).getDescripcion(), films.get(i).getAnno(), films.get(i).getLanguage(),films.get(i).getDuracion(),films.get(i).getPrecioAlquiler(),films.get(i).getPrecioVenta()};
			model.addRow(datos);
		}
		
		if(e.getActionCommand()==">") {
			System.out.println(model.getRowCount());
			while(model.getRowCount()!=0) {
				model.removeRow(0);
			}
			paginaactual++;
			
			for(int i=numregistros*paginaactual, nregistros=i+numregistros;i<nregistros;i++) {
				Object[] datos = new Object[] { films.get(i).getId(), films.get(i).getTitulo(),
						films.get(i).getDescripcion(), films.get(i).getAnno(), films.get(i).getLanguage(),films.get(i).getDuracion(),films.get(i).getPrecioAlquiler(),films.get(i).getPrecioVenta()};
				model.addRow(datos);
				if(i>=registros-1) {
					break;
				}
			}
			if(paginaactual==paginas) {
				this.siguiente.setEnabled(false);
			}
			this.atras.setEnabled(true);
			System.out.println(paginaactual);
		}
		if(e.getActionCommand()=="<") {
			System.out.println(model.getRowCount());
			while(model.getRowCount()!=0) {
				model.removeRow(0);
			}
			paginaactual--;
			
			for(int i=numregistros*paginaactual, nregistros=i+numregistros;i<nregistros;i++) {
				Object[] datos = new Object[] { films.get(i).getId(), films.get(i).getTitulo(),
						films.get(i).getDescripcion(), films.get(i).getAnno(), films.get(i).getLanguage(),films.get(i).getDuracion(),films.get(i).getPrecioAlquiler(),films.get(i).getPrecioVenta()};
				model.addRow(datos);
				if(i>=registros-1) {
					break;
				}
			}
			if(paginaactual==0) {
				this.atras.setEnabled(false);
			}
		}
		if(e.getActionCommand()==">>") {
			while(model.getRowCount()!=0) {
				model.removeRow(0);
			}
			paginaactual=paginas;
			
			for(int i=numregistros*paginaactual, nregistros=i+numregistros;i<nregistros;i++) {
				Object[] datos = new Object[] { films.get(i).getId(), films.get(i).getTitulo(),
						films.get(i).getDescripcion(), films.get(i).getAnno(), films.get(i).getLanguage(),films.get(i).getDuracion(),films.get(i).getPrecioAlquiler(),films.get(i).getPrecioVenta()};
				model.addRow(datos);
				if(i>=registros-1) {
					break;
				}
			}
				this.siguiente.setEnabled(false);
				this.atras.setEnabled(true);
		}
		if(e.getActionCommand()=="<<") {
			while(model.getRowCount()!=0) {
				model.removeRow(0);
			}
			paginaactual=0;
			
			for(int i=numregistros*paginaactual, nregistros=i+numregistros;i<nregistros;i++) {
				Object[] datos = new Object[] { films.get(i).getId(), films.get(i).getTitulo(),
						films.get(i).getDescripcion(), films.get(i).getAnno(), films.get(i).getLanguage(),films.get(i).getDuracion(),films.get(i).getPrecioAlquiler(),films.get(i).getPrecioVenta()};
				model.addRow(datos);
				if(i>=registros) {
					break;
				}
				
			}
				this.atras.setEnabled(false);
		}
		
	}
}
