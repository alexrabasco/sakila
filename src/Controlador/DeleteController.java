package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.EOFException;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Modelo.ActorModel;
import Modelo.CategoryModel;
import Modelo.FilmModel;

public class DeleteController implements ActionListener, ListSelectionListener {
	private JButton borrar;
	private JTable tabla;

	public DeleteController(JButton borrar, JTable tabla) {
		setBorrar(borrar);
		setTabla(tabla);

	}

	public JButton getBorrar() {
		return borrar;
	}

	public void setBorrar(JButton borrar) {
		this.borrar = borrar;
	}

	public JTable getTabla() {
		return tabla;
	}

	public void setTabla(JTable tabla) {
		this.tabla = tabla;
	}

	@Override
	public void actionPerformed(ActionEvent e) {

		int fila = this.tabla.getSelectedRow();
		int columnas = this.tabla.getColumnCount();
		if (columnas == 8) {
			int id = (Integer) tabla.getValueAt(fila, 0);
			FilmModel.borrarPelicula(id);
			if (FilmModel.borrarPelicula(id) == false) {
				JOptionPane.showMessageDialog(null, "Fila borrada con éxito");
			}

		}

		if (columnas == 2) {
			int id = (Integer) tabla.getValueAt(fila, 0);
			CategoryModel.borrarCategoria(id);
			if (CategoryModel.borrarCategoria(id) == false) {
				JOptionPane.showMessageDialog(null, "Fila borrada con éxito");
			}
		}

		if (columnas == 3) {
			int id = (Integer) tabla.getValueAt(fila, 0);
			ActorModel.borrarActor(id);
			if (ActorModel.borrarActor(id) == false) {
				JOptionPane.showMessageDialog(null, "Fila borrada con éxito");
			}

		}
		System.out.println(tabla.getValueAt(fila, 1) + " Ha sido borrado");

	}

	@Override
	public void valueChanged(ListSelectionEvent e) {
		if(this.tabla.getSelectedRow()==-1) {
			this.borrar.setEnabled(false);
		} else {
			this.borrar.setEnabled(true);
		}
	}
}
