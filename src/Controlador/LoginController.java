package Controlador;

import javax.swing.JButton;
import javax.swing.JFormattedTextField;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.SwingUtilities;

import Modelo.LoginModel;
import Vista.Index;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


public class LoginController implements ActionListener {
	private JFormattedTextField user;
	private JPasswordField pass;
	private JButton boton;
	
	public LoginController(JFormattedTextField user, JPasswordField pass, JButton boton) {
		setUser(user);
		setPass(pass);
		setButton(boton);
		
	}
	public JFormattedTextField getUser() {
		return user;
	}
	public void setUser(JFormattedTextField user) {
		this.user=user;
	}
	public JPasswordField setPass() {
		return pass;
	}
	public void setPass(JPasswordField pass) {
		this.pass=pass;
	}
	public JButton getBoton() {
		return boton;
	}
	public void setButton(JButton boton) {
		this.boton=boton;
	}
	
	
	public boolean verificar() {
		boolean v=false;
		LoginModel lm = new LoginModel(this.user.getText(),new String(this.pass.getPassword()));
		if(lm.httpRequest()==true) {
			v=true;
		}
		return v;
	}
	public boolean getEstado() {
		boolean estado=false;
		LoginModel lm = new LoginModel(this.user.getText(),new String(this.pass.getPassword()));
		if(lm.httpRequest()==true) {
			estado=true;
		} else {
			estado=false;
		}
		return estado;
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		LoginModel lm = new LoginModel(this.user.getText(),new String(this.pass.getPassword()));
		if(lm.httpRequest()==true) {
			Index i = new Index();
			i.setVisible(true);
			SwingUtilities.getWindowAncestor(this.getBoton()).dispose();
		} else {
			JOptionPane.showMessageDialog(null, "¡Error al loguear!");
		}	
		
	}
	
}

