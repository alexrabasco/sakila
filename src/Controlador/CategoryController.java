package Controlador;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import Modelo.CategoryModel;

public class CategoryController implements ActionListener {
	private JTable tabla;
	private JLabel titulo;
	private JComboBox<String> numregistros;
	private JButton siguiente;
	private JButton atras;
	private JButton ultimo;
	private JButton primero;
	private JButton siguientefilm;
	private JButton atrasfilm;
	private JButton ultimofilm;
	private JButton primerofilm;
	private JButton siguienteactor;
	private JButton atrasactor;
	private JButton ultimoactor;
	private JButton primeroactor;
	int paginaactual;
	
	public CategoryController(JTable tabla, JLabel titulo, JComboBox<String> numregistros,  JButton  siguiente,  JButton atras, JButton ultimo, JButton primero,
			JButton  siguientefilm,  JButton atrasfilm, JButton ultimofilm, JButton primerofilm,
			JButton  siguienteactor,  JButton atrasactor, JButton ultimoactor, JButton primeroactor) {
		setTabla(tabla);
		setTitulo(titulo);
		setNumRegistros(numregistros);
		setSiguiente(siguiente);
		setAtras(atras);
		setPrimero(primero);
		setUltimo(ultimo);
		
		setSiguientefilm(siguientefilm);
		setAtrasfilm(atrasfilm);
		setPrimerofilm(primerofilm);
		setUltimofilm(ultimofilm);
		
		setSiguienteactor(siguienteactor);
		setAtrasactor(atrasactor);
		setPrimeroactor(primeroactor);
		setUltimoactor(ultimoactor);
		
	}
	public JTable getTabla() {
		return tabla;
	}
	public void setTabla(JTable tabla) {
		this.tabla=tabla;
	}
	public JLabel getTitulo() {
		return titulo;
	}
	public void setTitulo(JLabel titulo) {
		this.titulo=titulo;
	}
	public JComboBox<String> getNumRegistros(){
		return numregistros;
	}
	public void setNumRegistros(JComboBox<String> numregistros) {
		this.numregistros=numregistros;
	}
	public JButton getSiguiente() {
		return siguiente;
	}
	public void setSiguiente( JButton siguiente ) {
		this.siguiente=siguiente;
	}
	public JButton getAtras() {
		return atras;
	}
	public void setAtras(JButton atras) {
		this.atras=atras;
	}
	public JButton getPrimero() {
		return primero;
	}
	public void setPrimero(JButton primero) {
		this.primero=primero;
	}
	public JButton getUltimo() {
		return ultimo;
	}
	public void setUltimo(JButton ultimo) {
		this.ultimo=ultimo;
	}
	//***************************************
	public JButton getSiguientefilm() {
		return siguientefilm;
	}
	public void setSiguientefilm( JButton siguientefilm ) {
		this.siguientefilm=siguientefilm;
	}
	public JButton getAtrasfilm() {
		return atrasfilm;
	}
	public void setAtrasfilm(JButton atrasfilm) {
		this.atrasfilm=atrasfilm;
	}
	public JButton getPrimerofilm() {
		return primerofilm;
	}
	public void setPrimerofilm(JButton primerofilm) {
		this.primerofilm=primerofilm;
	}
	public JButton getUltimofilm() {
		return ultimofilm;
	}
	public void setUltimofilm(JButton ultimofilm) {
		this.ultimofilm=ultimofilm;
	}
	//****************************************
	public JButton getSiguienteactor() {
		return siguienteactor;
	}
	public void setSiguienteactor( JButton siguienteactor ) {
		this.siguienteactor=siguienteactor;
	}
	public JButton getAtrasactor() {
		return atrasactor;
	}
	public void setAtrasactor(JButton atrasactor) {
		this.atrasactor=atrasactor;
	}
	public JButton getPrimeroactor() {
		return primeroactor;
	}
	public void setPrimeroactor(JButton primeroactor) {
		this.primeroactor=primeroactor;
	}
	public JButton getUltimoactor() {
		return ultimoactor;
	}
	public void setUltimoactor(JButton ultimoactor) {
		this.ultimoactor=ultimoactor;
	}
	@Override
	public void actionPerformed(ActionEvent e) {
		
		this.siguientefilm.setVisible(false);
		this.atrasfilm.setVisible(false);
		this.ultimofilm.setVisible(false);
		this.primerofilm.setVisible(false);
		
		this.siguienteactor.setVisible(false);
		this.atrasactor.setVisible(false);
		this.ultimoactor.setVisible(false);
		this.primeroactor.setVisible(false);
		
		this.siguiente.setVisible(true);
		this.atras.setVisible(true);
		this.ultimo.setVisible(true);
		this.primero.setVisible(true);
		
		this.siguiente.addActionListener(this);
		DefaultTableModel model = new DefaultTableModel();
		this.titulo.setText("Lista de categorías");
		this.tabla.removeAll();
		this.tabla.setModel(model);
		model.addColumn("Id");
		model.addColumn("Nombre");
		ArrayList<CategoryModel> categorias = CategoryModel.obtenerCategorias();
		
		
		this.atras.setEnabled(true);
		int registros = categorias.size();
		int numregistros = Integer.parseInt(this.numregistros.getSelectedItem().toString());
		int paginas=registros/numregistros;
		if(paginaactual!=0) {
			this.siguiente.setEnabled(true);
		}
		if(paginaactual!=0) {
			this.atras.setEnabled(true);
		}

		//*********
		for(int i=0;i<10;i++) {
			Object[] datos = new Object[]{categorias.get(i).getId(),categorias.get(i).getNombre()};
			model.addRow(datos);
		}
		
		if(e.getActionCommand()==">") {
			while(model.getRowCount()!=0) {
				model.removeRow(0);
			}
			paginaactual++;
			
			for(int i=numregistros*paginaactual, nregistros=i+numregistros;i<nregistros;i++) {
				Object[] datos = new Object[]{categorias.get(i).getId(),categorias.get(i).getNombre()};
				model.addRow(datos);
				if(i>=registros) {
					break;
				}
			}
			if(paginaactual==paginas) {
				this.siguiente.setEnabled(false);
			}
			this.atras.setEnabled(true);
		}
		if(e.getActionCommand()=="<") {
			while(model.getRowCount()!=0) {
				model.removeRow(0);
			}
			paginaactual--;
			
			for(int i=numregistros*paginaactual, nregistros=i+numregistros;i<nregistros;i++) {
				Object[] datos = new Object[]{categorias.get(i).getId(),categorias.get(i).getNombre()};
				model.addRow(datos);
				if(i>=registros-1) {
					break;
				}
			}
			if(paginaactual==0) {
				this.atras.setEnabled(false);
			}
		}
		if(e.getActionCommand()==">>") {
			while(model.getRowCount()!=0) {
				model.removeRow(0);
			}
			paginaactual=paginas;
			
			for(int i=numregistros*paginaactual, nregistros=i+numregistros;i<nregistros;i++) {
				Object[] datos = new Object[]{categorias.get(i).getId(),categorias.get(i).getNombre()};
				model.addRow(datos);
				if(i>=registros-1) {
					break;
				}
			}
				this.siguiente.setEnabled(false);
				this.atras.setEnabled(true);
		}
		if(e.getActionCommand()=="<<") {
			while(model.getRowCount()!=0) {
				model.removeRow(0);
			}
			paginaactual=0;
			
			for(int i=numregistros*paginaactual, nregistros=i+numregistros;i<nregistros;i++) {
				Object[] datos = new Object[]{categorias.get(i).getId(),categorias.get(i).getNombre()};
				model.addRow(datos);
				if(i>=registros) {
					break;
				}
				
			}
				this.atras.setEnabled(false);
		}
	}
		
	}


