package Vista;

import java.awt.EventQueue;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.LoginController;

import javax.swing.JFormattedTextField;
import java.awt.Color;

import javax.swing.JPasswordField;
import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.Image;
import java.awt.event.ActionEvent;

import javax.swing.ImageIcon;
import javax.swing.JSeparator;
import javax.swing.JCheckBox;
import java.net.*;
import java.util.Scanner;

public class LoginView extends JFrame  {

	public JPanel contentPane;
	
	File file = new File("save.txt");

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

	}

	/**
	 * Create the frame.
	 */
	public LoginView() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 652, 414);
		contentPane = new JPanel();
		contentPane.setBackground(Color.WHITE);
		contentPane.setForeground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		
		
		JFormattedTextField txtfielduser = new JFormattedTextField();
		txtfielduser.setBorder(null);
		txtfielduser.setDisabledTextColor(Color.WHITE);
		txtfielduser.setSelectedTextColor(Color.BLACK);
		txtfielduser.setCaretColor(Color.BLACK);
		txtfielduser.setBackground(Color.WHITE);
		txtfielduser.setForeground(Color.BLACK);
		txtfielduser.setBounds(423, 163, 182, 26);
		
		JPasswordField passwordField = new JPasswordField();
		passwordField.setSelectionColor(Color.BLACK);
		passwordField.setDisabledTextColor(Color.BLACK);
		passwordField.setBorder(null);
		passwordField.setBounds(423, 246, 193, 26);
		
		JButton btnIniciarSesion = new JButton("");
		btnIniciarSesion.setName("");
		btnIniciarSesion.setForeground(Color.WHITE);
		btnIniciarSesion.setIconTextGap(0);
		btnIniciarSesion.setBorderPainted(false);
		btnIniciarSesion.setBorder(null);
		btnIniciarSesion.setBounds(396, 338, 209, 41);
		contentPane.setLayout(null);
		ImageIcon btnicon = new ImageIcon("imagenes/iconoboton.png");
		Icon iconoboton = new ImageIcon(btnicon.getImage().getScaledInstance(btnIniciarSesion.getWidth()-25, btnIniciarSesion.getHeight(), Image.SCALE_DEFAULT));
		btnIniciarSesion.setIcon(iconoboton);
		
		contentPane.add(btnIniciarSesion);
		contentPane.add(txtfielduser);
		contentPane.add(passwordField);
		
		
		
		passwordField.setOpaque(true);
		ImageIcon foto = new ImageIcon("imagenes/fondorojo.jpg");
		ImageIcon usericon = new ImageIcon("imagenes/user.png");
		ImageIcon passwordicon = new ImageIcon("imagenes/password.png");
		ImageIcon textlogoicon = new ImageIcon("imagenes/textlogo.png");
		
		JLabel lblNewLabel = new JLabel("Holaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa");
		lblNewLabel.setBackground(Color.BLACK);
		lblNewLabel.setBounds(0, -12, 331, 404);
		Icon icono = new ImageIcon(foto.getImage().getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(), Image.SCALE_DEFAULT));
		lblNewLabel.setIcon(icono);
		contentPane.add(lblNewLabel);
		
		JLabel labeluser = new JLabel("HOLAAAAAAAAAAAAAA");
		labeluser.setBounds(342, 130, 69, 62);
		Icon iconouser = new ImageIcon(usericon.getImage().getScaledInstance(labeluser.getWidth(), labeluser.getHeight(), Image.SCALE_DEFAULT));
		labeluser.setIcon(iconouser);
		contentPane.add(labeluser);
		
		JLabel labelpassword = new JLabel("holaaaaaaaa");
		labelpassword.setBounds(342, 224, 69, 48);
		Icon iconopassword = new ImageIcon(passwordicon.getImage().getScaledInstance(labelpassword.getWidth(), labelpassword.getHeight(), Image.SCALE_DEFAULT));
		labelpassword.setIcon(iconopassword);
		contentPane.add(labelpassword);
		
		JSeparator separator = new JSeparator();
		separator.setBounds(423, 268, 182, 16);
		contentPane.add(separator);
		
		JSeparator separator_1 = new JSeparator();
		separator_1.setBounds(423, 187, 182, 16);
		contentPane.add(separator_1);
		
		JLabel textlogolabel = new JLabel("New label");
		textlogolabel.setBounds(351, 6, 280, 123);
		Icon iconologo = new ImageIcon(textlogoicon.getImage().getScaledInstance(textlogolabel.getWidth(), textlogolabel.getHeight(), Image.SCALE_DEFAULT));
		textlogolabel.setIcon(iconologo);
		contentPane.add(textlogolabel);
		
		JCheckBox cbRecordar = new JCheckBox("Recordar mis credenciales");
		cbRecordar.setBounds(396, 303, 209, 23);
		contentPane.add(cbRecordar);
		//********************************************************************
		LoginController lg = new LoginController(txtfielduser, passwordField,btnIniciarSesion);
		this.dispose();
		cbRecordar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				boolean verificar;
				verificar=lg.verificar();
				if(verificar==true) {
					if(cbRecordar.isSelected()) {	
				        try {
				            if(!file.exists()) file.createNewFile();  //SI EL ARCHIVO NO EXISTE CREA UNO NUEVO

				            BufferedWriter bw = new BufferedWriter(new FileWriter(file.getAbsolutePath()));
				            bw.write(txtfielduser.getText()); 
				            bw.newLine(); 
				            bw.write(new String(passwordField.getPassword())); 
				            bw.close(); 

				        } catch (IOException exception) {
				        	exception.printStackTrace(); 
				        	} 
					} else {
						file.delete();
					}
					
				}
				
			}
		});
		try {
	          if(file.exists()){    //ENTRA SI EL ARCHIVO EXISTE

	            Scanner scan = new Scanner(file);   //USAMOS EL ESCANER PARA LEER EL FICHERO

	            txtfielduser.setText(scan.nextLine());  
	            passwordField.setText(scan.nextLine()); 
	            cbRecordar.setSelected(true);
	            scan.close();
	          }

	        } catch (FileNotFoundException e) {         
	            e.printStackTrace();
	        }

		btnIniciarSesion.addActionListener(lg);
		if(lg.getEstado()==true) {
			this.setVisible(false);
		} else {
			this.dispose();
		}
		

	}

}
