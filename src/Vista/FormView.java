package Vista;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Controlador.InsertController;

import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JButton;

public class FormView extends JFrame {

	private JPanel contentPane;
	private JTextField tfId;
	private JTextField tfTitulo;
	private JTextField tfDescripcion;
	private JTextField tfAnno;
	private JTextField tfduracion;
	private JTextField tfprecioalquiler;
	private JTextField tfprecioventa;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {

	}

	/**
	 * Create the frame.
	 */
	public FormView() {
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 431, 794);
		contentPane = new JPanel();
		contentPane.setBackground(Color.BLACK);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

		
		JLabel elegirtabla = new JLabel("Elija la tabla deseada para operar");
		elegirtabla.setForeground(Color.WHITE);
		elegirtabla.setBounds(111, 29, 215, 16);
		elegirtabla.setName("elegirtabla"); //************************************> nombre: elegirtabla
		contentPane.add(elegirtabla);
		
		JLabel labelId = new JLabel("Id");
		labelId.setForeground(Color.WHITE);
		labelId.setBounds(111, 136, 158, 16);
		labelId.setName("labelId");
		contentPane.add(labelId);
		
		tfId = new JTextField();
		tfId.setBounds(111, 164, 215, 26);
		contentPane.add(tfId);
		tfId.setName("tfid");
		tfId.setColumns(10);
		
		JLabel lblTitulo = new JLabel("Titulo");
		lblTitulo.setForeground(Color.WHITE);
		lblTitulo.setBounds(111, 202, 149, 16);
		lblTitulo.setName("lblTitulo");
		contentPane.add(lblTitulo);
		
		tfTitulo = new JTextField();
		tfTitulo.setBounds(111, 230, 215, 26);
		tfTitulo.setName("tftitulo");
		contentPane.add(tfTitulo);
		tfTitulo.setColumns(10);
		
		JLabel lblDescripcion = new JLabel("Descripción");
		lblDescripcion.setForeground(Color.WHITE);
		lblDescripcion.setName("lblDescripcion");
		lblDescripcion.setBounds(111, 268, 158, 16);
		contentPane.add(lblDescripcion);
		
		tfDescripcion = new JTextField();
		tfDescripcion.setName("tfdescripcion");
		tfDescripcion.setBounds(111, 296, 215, 26);
		contentPane.add(tfDescripcion);
		tfDescripcion.setColumns(10);
		
		JLabel lblAoDeLanzamiento = new JLabel("Año de lanzamiento");
		lblAoDeLanzamiento.setForeground(Color.WHITE);
		lblAoDeLanzamiento.setName("lblAoDeLanzamiento");
		lblAoDeLanzamiento.setBounds(111, 334, 158, 16);
		contentPane.add(lblAoDeLanzamiento);
		
		tfAnno = new JTextField();
		tfAnno.setName("tfanno");
		tfAnno.setBounds(111, 362, 215, 26);
		contentPane.add(tfAnno);
		tfAnno.setColumns(10);
		
		JButton btnEjecutarQuery = new JButton("Insertar");
		btnEjecutarQuery.setName("btninsertar");
		btnEjecutarQuery.setBounds(144, 610, 149, 40);
		contentPane.add(btnEjecutarQuery);
		
		JComboBox<String> comboIdiomas = new JComboBox<String>();
		comboIdiomas.setName("comboidiomas");
		comboIdiomas.setBounds(159, 108, 110, 27);
		comboIdiomas.addItem("English");
		comboIdiomas.addItem("Italian");
		comboIdiomas.addItem("Japanese");
		comboIdiomas.addItem("Mandarin");
		comboIdiomas.addItem("French");
		comboIdiomas.addItem("German");
		
		JButton btnUpdate = new JButton("Actualizar");
		btnUpdate.setBounds(144, 610, 149, 35);
		btnUpdate.setName("btnupdate");
		contentPane.add(btnUpdate);
		btnUpdate.setVisible(false);
		contentPane.add(comboIdiomas);
		
		JComboBox<String> comboTablas = new JComboBox<String>();
		comboTablas.setName("combotablas");
		comboTablas.addItem("film");
		comboTablas.addItem("actor");
		comboTablas.addItem("category");
		comboTablas.setBounds(159, 57, 110, 27);
		contentPane.add(comboTablas);
		
		JLabel lblDuracion = new JLabel("Duración ");
		lblDuracion.setForeground(Color.WHITE);
		lblDuracion.setName("lblduracion");
		lblDuracion.setBounds(111, 400, 61, 16);
		contentPane.add(lblDuracion);
		
		tfduracion = new JTextField();
		tfduracion.setName("tfduracion");
		tfduracion.setBounds(111, 422, 215, 26);
		contentPane.add(tfduracion);
		tfduracion.setColumns(10);
		
		tfprecioalquiler = new JTextField();
		tfprecioalquiler.setName("tfprecioalquiler");
		tfprecioalquiler.setBounds(111, 481, 215, 26);
		contentPane.add(tfprecioalquiler);
		tfprecioalquiler.setColumns(10);
		
		JLabel lblPrecioDeVenta = new JLabel("Precio de venta");
		lblPrecioDeVenta.setForeground(Color.WHITE);
		lblPrecioDeVenta.setName("lblprecioventa");//***************************
		lblPrecioDeVenta.setBounds(111, 519, 110, 16);
		contentPane.add(lblPrecioDeVenta);
		
		JLabel lblPrecioAlquiler = new JLabel("Precio de alquiler");
		lblPrecioAlquiler.setForeground(Color.WHITE);
		lblPrecioAlquiler.setName("lblalquiler");//******************************
		lblPrecioAlquiler.setBounds(111, 460, 149, 16);
		contentPane.add(lblPrecioAlquiler);
		
		tfprecioventa = new JTextField();
		tfprecioventa.setName("tfprecioventa");//*********************************
		tfprecioventa.setBounds(111, 547, 215, 26);
		contentPane.add(tfprecioventa);
		tfprecioventa.setColumns(10);
		
		
		
		JLabel lblLenguajeDePelicula = new JLabel("Lenguaje de pelicula");
		lblLenguajeDePelicula.setName("lblLenguajeDePelicula");
		lblLenguajeDePelicula.setForeground(Color.WHITE);
		lblLenguajeDePelicula.setBounds(144, 84, 149, 16);
		contentPane.add(lblLenguajeDePelicula);
		
		InsertController ic = new InsertController(comboTablas,comboIdiomas,labelId, lblTitulo,lblDescripcion, lblAoDeLanzamiento, tfId, tfTitulo, tfDescripcion, tfAnno,
				lblLenguajeDePelicula,lblDuracion, lblPrecioAlquiler, lblPrecioDeVenta, tfduracion, tfprecioalquiler, tfprecioventa, this);
		comboTablas.addItemListener(ic);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(FormView.class.getResource("/Vista/redmenubackground.jpg")));
		label.setOpaque(true);
		label.setBackground(Color.BLACK);
		label.setBounds(73, 18, 275, 682);
		contentPane.add(label);
		
		JLabel lblNewLabel_2 = new JLabel("New label");
		lblNewLabel_2.setOpaque(true);
		lblNewLabel_2.setBackground(Color.BLACK);
		lblNewLabel_2.setBounds(61, 6, 301, 710);
		contentPane.add(lblNewLabel_2);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(FormView.class.getResource("/Vista/formbackground.jpg")));
		lblNewLabel_1.setBackground(Color.BLACK);
		lblNewLabel_1.setBounds(0, 0, 431, 772);
		contentPane.add(lblNewLabel_1);
		//*********
		btnEjecutarQuery.addActionListener(ic);
		for(Component componente: contentPane.getComponents()) {
			System.out.println(componente.getName());
		}
	}
}
