package Vista;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import Controlador.ActorController;
import Controlador.BuscadorController;
import Controlador.CategoryController;
import Controlador.DeleteController;
import Controlador.FilmController;
import Controlador.UpdateController;
import Modelo.FilmModel;

import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JRadioButtonMenuItem;
import javax.swing.JLabel;
import javax.swing.ImageIcon;
import java.awt.Color;
import javax.swing.JTable;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.ScrollPane;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSeparator;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.border.BevelBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.JComboBox;

public class Index extends JFrame {

	private JPanel contentPane;
	String col [] = {"Id","Titulo","Descripcion","Año de lanzamiento" };
	DefaultTableModel model = new DefaultTableModel();
	private JTable tablalistar;
	private JTextField tfbuscar;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		Index in1 = new Index();
		in1.setVisible(true);
	}
	/**
	 * Create the frame.
	 */
	public Index() {
		setResizable(false);
		setBackground(Color.GRAY);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 812, 539);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(139, 0, 0));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblTitulo = new JLabel("Lista de películas");
		lblTitulo.setBackground(Color.BLACK);
		lblTitulo.setFont(new Font("Krungthep", Font.PLAIN, 24));
		lblTitulo.setForeground(Color.WHITE);
		lblTitulo.setBounds(347, 3, 265, 29);
		contentPane.add(lblTitulo);
		
		
		JButton btnInsertar = new JButton("Insertar");
		btnInsertar.setBackground(new Color(255, 255, 255));
		btnInsertar.setForeground(new Color(0, 0, 0));
		btnInsertar.setBounds(6, 272, 162, 44);
		btnInsertar.setFont(new Font("Lucida Grande", Font.PLAIN, 13));
		contentPane.add(btnInsertar);
		btnInsertar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				FormView fv = new FormView();
				fv.setVisible(true);
				
			}
		});
		
		
		//FilmController fc = new FilmController(table);

		
		JButton btnprimerapagfilm = new JButton("<<");
		btnprimerapagfilm.setBounds(180, 424, 39, 29);
		contentPane.add(btnprimerapagfilm);
		
		JButton btnprimerapagcategoy = new JButton("<<");
		btnprimerapagcategoy.setBounds(180, 424, 39, 29);
		contentPane.add(btnprimerapagcategoy);
		
		JButton btnprimerapagactor = new JButton("<<");
		btnprimerapagactor.setBounds(180, 424, 39, 29);
		contentPane.add(btnprimerapagactor);
		
		JButton btnanteriorfilm = new JButton("<");
		btnanteriorfilm.setBounds(231, 424, 39, 29);
		contentPane.add(btnanteriorfilm);
		
		JButton btnanteriorcategory = new JButton("<");
		btnanteriorcategory.setBounds(231, 424, 39, 29);
		contentPane.add(btnanteriorcategory);
		btnanteriorcategory.setVisible(false);
		
		JButton btnanterioractor = new JButton("<");
		btnanterioractor.setBounds(231, 424, 39, 29);
		contentPane.add(btnanterioractor);
		btnanterioractor.setVisible(false);
		
		
		JButton btnsiguientefilm = new JButton(">");
		btnsiguientefilm.setBounds(282, 424, 39, 29);
		contentPane.add(btnsiguientefilm);
		
		JButton btnsiguientecategory = new JButton(">");
		btnsiguientecategory.setBounds(282, 424, 39, 29);
		contentPane.add(btnsiguientecategory);
		btnsiguientecategory.setVisible(false);
		
		JButton btnsiguienteactor = new JButton(">");
		btnsiguienteactor.setBounds(282, 424, 39, 29);
		contentPane.add(btnsiguienteactor);
		btnsiguienteactor.setVisible(false);
		
		
		JButton btnultimapagfilm = new JButton(">>");
		btnultimapagfilm.setBounds(333, 424, 39, 29);
		contentPane.add(btnultimapagfilm);
		
		JButton btnultimapagcategory = new JButton(">>");
		btnultimapagcategory.setBounds(333, 424, 39, 29);
		contentPane.add(btnultimapagcategory);
		btnultimapagcategory.setVisible(false);
		
		JButton btnultimapagactor = new JButton(">>");
		btnultimapagactor.setBounds(333, 424, 39, 29);
		contentPane.add(btnultimapagactor);
		btnultimapagactor.setVisible(false);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setEnabled(false);
		scrollPane.setBounds(180, 34, 626, 378);
		contentPane.add(scrollPane);
		
		tablalistar = new JTable();
		tablalistar.setBackground(Color.WHITE);
		scrollPane.setViewportView(tablalistar);
		
		tfbuscar = new JTextField();
		tfbuscar.setBounds(282, 459, 211, 29);
		contentPane.add(tfbuscar);
		tfbuscar.setColumns(10);
		

		
		JButton btnSearch = new JButton("Buscar");
		btnSearch.setFont(new Font("Helvetica", Font.PLAIN, 18));
		btnSearch.setBackground(Color.WHITE);
		btnSearch.setBounds(511, 460, 117, 29);
		contentPane.add(btnSearch);
		
		
		JComboBox<String> combonumeroregistros = new JComboBox<String>();
		combonumeroregistros.addItem("10");
		combonumeroregistros.addItem("15");
		combonumeroregistros.addItem("20");
		combonumeroregistros.setSelectedItem(10);
		combonumeroregistros.setBounds(384, 425, 109, 27);
		contentPane.add(combonumeroregistros);
		
		
		FilmController fc = new FilmController(tablalistar, lblTitulo,combonumeroregistros, btnsiguientefilm, btnanteriorfilm, btnultimapagfilm, btnprimerapagfilm,
				btnsiguienteactor, btnanterioractor, btnultimapagactor, btnprimerapagactor,
				btnsiguientecategory, btnanteriorcategory, btnultimapagcategory
				, btnprimerapagcategoy);
		
		JButton btnPeliculas = new JButton("Tabla peliculas");
		btnPeliculas.setBounds(6, 10, 162, 44);
		btnPeliculas.addActionListener(fc);
		
		JComboBox<String> combobusqueda = new JComboBox<String>();
		combobusqueda.setBounds(180, 461, 90, 27);
		combobusqueda.addItem("Id");
		combobusqueda.addItem("Nombre");
		contentPane.add(combobusqueda);
		contentPane.add(btnPeliculas);
		
		BuscadorController bc = new BuscadorController(tablalistar, combobusqueda);
		btnSearch.addActionListener(bc);
		
		ActorController ac = new ActorController(tablalistar, lblTitulo, combonumeroregistros,btnsiguienteactor, btnanterioractor, btnultimapagactor, btnprimerapagactor,
				btnsiguientefilm, btnanteriorfilm, btnultimapagfilm, btnprimerapagfilm,
				btnsiguientecategory, btnanteriorcategory, btnultimapagcategory
				, btnprimerapagcategoy);
		
		JButton btnActores = new JButton("Tabla actores");
		btnActores.setBounds(6, 54, 162, 44);
		btnActores.addActionListener(ac);
		contentPane.add(btnActores);
		
		JButton btnCategorias = new JButton("Tabla categorías");
		CategoryController cc = new CategoryController(tablalistar, lblTitulo,combonumeroregistros,btnsiguientecategory, btnanteriorcategory, btnultimapagcategory
				, btnprimerapagcategoy, btnsiguientefilm, btnanteriorfilm, btnultimapagfilm, btnprimerapagfilm, 
				btnsiguienteactor, btnanterioractor, btnultimapagactor, btnprimerapagactor);
		btnCategorias.setBounds(6, 99, 162, 44);
		btnCategorias.addActionListener(cc);
		contentPane.add(btnCategorias);
		
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.setBounds(6, 328, 162, 44);
		contentPane.add(btnBorrar);
		DeleteController dc = new DeleteController(btnBorrar,tablalistar);
		btnBorrar.addActionListener(dc);
		
		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.setBounds(6, 384, 162, 44);
		btnActualizar.setEnabled(false);
		contentPane.add(btnActualizar);
		btnBorrar.setEnabled(false);
		
		FormView formulario = new FormView();
		UpdateController actualizar = new UpdateController(formulario, tablalistar, btnActualizar, this);
		btnActualizar.addActionListener(actualizar);
		tablalistar.getSelectionModel().addListSelectionListener(actualizar);
		tablalistar.getSelectionModel().addListSelectionListener(dc);

		btnsiguientefilm.addActionListener(fc);
		btnanteriorfilm.addActionListener(fc);
		btnultimapagfilm.addActionListener(fc);
		btnprimerapagfilm.addActionListener(fc);
		
		btnsiguienteactor.addActionListener(ac);
		btnanterioractor.addActionListener(ac);
		btnultimapagactor.addActionListener(ac);
		btnprimerapagactor.addActionListener(ac);
		
		btnsiguientecategory.addActionListener(cc);
		btnanteriorcategory.addActionListener(cc);
		btnultimapagcategory.addActionListener(cc);
		btnprimerapagcategoy.addActionListener(cc);
		
		
		
		JLabel lblfondo = new JLabel("New label");
		lblfondo.setIcon(new ImageIcon(Index.class.getResource("/Vista/indexbackground.jpg")));
		lblfondo.setBounds(0, 0, 812, 517);
		contentPane.add(lblfondo);
		fc.listar();
		
	}
}
